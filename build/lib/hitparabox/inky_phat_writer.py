#!/usr/bin/env python

import argparse
import os

from PIL import Image, ImageFont, ImageDraw
from font_hanken_grotesk import HankenGroteskBold, HankenGroteskMedium
from font_intuitive import Intuitive
from font_roboto import RobotoBold, RobotoMedium
from datetime import datetime
from inky.auto import auto


class inkyPhatWriter:
    def __init__(self):
        self.inky_display = auto()
        self.scale_size = 1
        self.padding = 0
        if self.inky_display.resolution == (400, 300):
            self.scale_size = 2.20
            self.padding = 15
        self.colour = self.inky_display.colour

        # inky_display.set_rotation(180)
        self.inky_display.set_border(self.inky_display.RED)

        # Load the fonts
        self.intuitive_font = ImageFont.truetype(Intuitive, int(22 * self.scale_size))
        self.hanken_bold_font = ImageFont.truetype(HankenGroteskBold, int(35 * self.scale_size))
        self.hanken_medium_font = ImageFont.truetype(HankenGroteskMedium, int(11 * self.scale_size))
        self.roboto_light = ImageFont.truetype(RobotoMedium, int(18 * self.scale_size))
        self.roboto_medium_small = ImageFont.truetype(RobotoMedium, int(11 * self.scale_size))
        self.img = None

    def print_song_list(self, song_artist_list, window_title="", delemiter="//", background_image=None):

        PATH = os.path.dirname(__file__)

        title_w, title_h = self.roboto_light.getsize(window_title)

        if background_image is None:
            self.img = Image.new("P", (self.inky_display.WIDTH, self.inky_display.HEIGHT))
        else:
            print(f"with image {os.path.join(PATH, background_image)}")
            self.img = Image.open(os.path.join(PATH, background_image))

        self.draw = ImageDraw.Draw(self.img)

        line_count = 0
        for obj in song_artist_list:
            artist = obj.get('artist')
            title = obj.get('title')
            line = artist + delemiter + title
            line_w, line_h = self.hanken_medium_font.getsize(line)
            self.draw.text((3, title_h + (line_h * line_count) + 3), line, self.inky_display.WHITE, font=self.hanken_medium_font)
            line_count = line_count + 1
            if line_count == 5:
                break

        time = datetime.now().strftime("%H:%M:%S")
        time_w, time_h = self.roboto_medium_small.getsize(time)

        y_top = int(self.inky_display.WHITE * (5.0 / 10.0))
        y_bottom = y_top + int(self.inky_display.HEIGHT * (4.0 / 10.0))

        title_x = int(3)
        title_y = int(0)
        self.draw.text((title_x, title_y), window_title, self.inky_display.RED, font=self.roboto_light)
        self.draw.text((self.inky_display.WIDTH/2 - time_w/2, self.inky_display.HEIGHT - time_h - 2), time, self.inky_display.BLACK, font=self.roboto_medium_small)

        self.inky_display.set_image(self.img)
        self.inky_display.show()
