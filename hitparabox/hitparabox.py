import urllib.request, json, time
from urllib.error import URLError

class Hitparabox:
    def __init__(self, song_url):
        self.__song_url = song_url
        self.__currentSongList = self.clean_up(self.get_song_list())

    def get_song_list_stream(self, update_function, stop_function, frequency=15):
        running = True
        update_function(json.dumps(self.__currentSongList))
        while running:
            song_list = self.get_song_list()
            new_song_list = self.clean_up(song_list)
            if self.song_list_update(new_song_list):
                update_function(json.dumps(self.__currentSongList))
            running = stop_function()
            time.sleep(frequency)
        return

    def get_song_list(self, raw=False):
        with urllib.request.urlopen(self.__song_url) as url:
            try:
                raw_song_list = url.read().decode()
            except URLError:
                print("URL Error better luck next time")

            if raw:
                return raw_song_list
            else:
                return json.loads(raw_song_list)

    def clean_up(self, song_list):
        for obj in song_list:
            try:
                del obj['stationId']
                del obj['id']
                del obj['type']
                del obj['playingMode']
                del obj['duration']
                del obj['starttime']
                del obj['url']
            except KeyError:
                pass
        return song_list

    def song_list_update(self, new_song_list):
        first_new = next(iter(new_song_list))
        first_current = next(iter(self.__currentSongList))
        if first_new.get('artist') == first_current.get('artist') and first_new.get('title') == first_current.get('title'):
            return False
        else:
            self.__currentSongList = new_song_list
            return True
